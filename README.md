# Jamf Connect Notify

Script for the Jamf Connect Login "Notify" tool

Use:
- Create a category used for policies to be executed by DEP Notify (ie. "Deployment"), define with Parameter 4
- Create a local API user account with the only privilege of "Policies=Read"
- Optionally encode the API user password with base64 outside this script. For example: echo password | base64
- Policies will be executed by id number, no need for custom triggers
- The name of the policy is used for the description on the progress bar. Name these policies something like "Installing MS Office"