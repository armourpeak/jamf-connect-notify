#!/bin/bash

#######################################
# © 2019 ARMOUR PEAK LLC - Brian Hanson
# API script that finds and executes all policies in a "deployment" category used for the "DEP Notify" feature in Jamf Connect.
#
# Use:
# - Create a category used for policies to be executed by DEP Notify (ie. "Deployment"), define with Parameter 4
# - Create a local API user account with the only privilege of "Policies=Read"
# - Encode the API credentials as base64 (username:password)
# - Policies will be executed by id number, no need for custom triggers
# - The name of the policy is used for the description on the progress bar. Name these policies something like "Installing MS Office"

######################################
# Start Configuration
######################################
CREDS="jfjeruie94958jJJSJdjJ33949djJJaoriowpovNNNXKSJjeuwi49e9jfnaJHJDKAZ"
######################################
# VARIABLES
######################################
SERVER=$(defaults read /Library/Preferences/com.jamfsoftware.jamf.plist jss_url)
#CATEGORY=Deployment
CATEGORY=$4
DEP_NOTIFY_LOG="/var/tmp/depnotify.log"
JAMF_BINARY="/usr/local/bin/jamf"
#######################################
# Create an XSLT template at /tmp/stylesheet.xslt
#######################################
cat << EOF > /tmp/stylesheet.xslt
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
<xsl:template match="/"> 
	<xsl:for-each select="policies/policy"> 
		<xsl:value-of select="id"/>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="name"/>
		<xsl:text>&#xa;</xsl:text> 
	</xsl:for-each> 
</xsl:template> 
</xsl:stylesheet>
EOF
#######################################

#######################################
# Set the time
sntp -sS time.apple.com
#######################################

curl -X GET "$SERVER"/JSSResource/policies/category/$CATEGORY \
	--silent \
	--header 'authorization: Basic '$CREDS \
	| xsltproc /tmp/stylesheet.xslt - > /tmp/deployments

TOTAL_POLICY_COUNT=$(echo $(wc -l < /tmp/deployments))
TOTAL_POLICY_COUNT=$((TOTAL_POLICY_COUNT+1))

# Change the MainText, since we set a generic message in the .pkg
echo "Command: MainText: Installing Software and Settings..." >> "$DEP_NOTIFY_LOG"

# Setup the progress bar
echo "Command: Determinate: $TOTAL_POLICY_COUNT" >> "$DEP_NOTIFY_LOG"

while read i; do
	ID=$(echo "$i" | awk -F'-' '{ print $1 }')
	DESC=$(echo "$i" | awk -F'-' '{ print $2 }')
	echo "Status: $DESC" >> "$DEP_NOTIFY_LOG"
	"$JAMF_BINARY" policy -id $ID
done < /tmp/deployments
rm /tmp/deployments

echo "Command: Quit" >> /var/tmp/depnotify.log

exit 0




